

Public Class Form1
  
    Private Sub TextBox1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TxtFilePath.MouseClick

        If ZipFileDialog.ShowDialog() = DialogResult.OK Then
            TxtFilePath.Text = ZipFileDialog.FileName
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Button1.Enabled = False
        BtnCancel.Enabled = True
        Label2.Hide()
        LinkLabel1.Hide()
        TxtFilePath.Enabled = False
        Dim remaining As Int16 = My.Settings.remaining
        ' remaining = 999
        'Check Remaining Days
        If My.Settings.remaining_days - Date.Today.Subtract(My.Settings.start).TotalDays() < 0 Then
            MsgBox("Sua licen�a expirou")
        Else
            If remaining > 0 Then
                'Launch New Thread
                BackgroundWorker1.WorkerReportsProgress = True
                BackgroundWorker1.WorkerSupportsCancellation = True
                BackgroundWorker1.RunWorkerAsync()
            Else
                MsgBox("Convers�es n�o restantes")
            End If
        End If
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MessageBox.Show("Tem certeza de que deseja fechar o exportador?", "Close", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Login.Close()

        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        Dim MyProcess As New Process()
        MyProcess.StartInfo.FileName = "explorer.exe"
        MyProcess.StartInfo.Arguments = "C:\temp\"
        MyProcess.Start()
        MyProcess.WaitForExit()
        MyProcess.Close()
        MyProcess.Dispose()
    End Sub

  

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim unzipper As Unzipper = New Unzipper(TxtFilePath.Text, BackgroundWorker1)

    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        lblStatus.Text = e.UserState.ToString
        ProgressBar1.Value = e.ProgressPercentage

        If e.UserState.ToString = "Convers�o terminada" Then
            Dim remaining As Int16 = My.Settings.remaining
            remaining = remaining - 1
            My.Settings.count = My.Settings.count + 1
            My.Settings.remaining = remaining
            My.Settings.Save()
            Label2.Visible = True
            LinkLabel1.Visible = True
            ProgressBar1.Value = 100
        End If
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted

        'MsgBox(lblStatus.Text)
        'Button1.Enabled = True
        'BtnCancel.Enabled = False
        'TxtFilePath.Enabled = True

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        If BackgroundWorker1.WorkerSupportsCancellation Then
            BackgroundWorker1.CancelAsync()
        End If

        Button1.Enabled = True
        BtnCancel.Enabled = False
        Label2.Hide()
        LinkLabel1.Hide()
        TxtFilePath.Enabled = True
    End Sub


  
  
End Class
