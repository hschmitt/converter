Imports ICSharpCode.SharpZipLib.Zip
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Text.RegularExpressions
Imports Redemption
Imports system.io

Public Class Unzipper
    Dim filenames As String()
    Dim zippedFile As String = ""
    Dim unzipper As New ICSharpCode.SharpZipLib.Zip.FastZip
    Dim unzipped_folder = "C:/temporary_converter"
    Dim exported_pst = "C:/temp/exported.pst"
    Dim exported_pst_correct = "C:\temp\exported.pst"
    Dim extra_path = "http_80"
    Dim backslash = "/"
    Dim database = "database-test#database"
    Dim attachment_folder = "test-store[1]#localserver/"
    Dim conn_string As String = ""
    Dim folder_name As String = "Inbox"
    Dim file_name_folder As String = "exporter"
    Dim ContentType As String
    Dim filenamecount = 0
    Dim worker As System.ComponentModel.BackgroundWorker
    Public Sub New(ByVal Value As String, ByRef bgWorker As System.ComponentModel.BackgroundWorker)
        Me.zippedFile = Value
        Me.worker = bgWorker
        Me.Start()
    End Sub
    Public Sub Start()

        If Me.zippedFile.Length > 0 Then
            '0 % Unzipping
            Me.worker.ReportProgress(0, "Descompactando")
            Me.Unzip()
            Me.worker.ReportProgress(15, "Pesquisando")
            '15% Searching 
            '20% Processing x/y    
            Me.SearchForSqlLiteFiles()

            Me.DeleteFiles()
            '100% Complete
        Else
            MsgBox("Invalid Zipped File")
        End If
    End Sub
    Private Sub Unzip()
        'Unzipping

        Me.file_name_folder = System.IO.Path.GetFileNameWithoutExtension(Me.zippedFile)
        Me.exported_pst = "C:/temp/" + Me.file_name_folder + ".pst"
        Me.exported_pst_correct = "C:\temp\" + Me.file_name_folder + ".pst"
        Me.unzipped_folder = Me.unzipped_folder
        unzipper.ExtractZip(Me.zippedFile, Me.unzipped_folder, "")




    End Sub

    Private Sub SearchForSqlLiteFiles()
        'Check if exists in parent
        Dim DirList As New ArrayList
        Dim item As String
        Dim db_file As String
        Dim exists As Boolean
        Dim attachment_files As String
        GetDirectories(Me.unzipped_folder, DirList)
        DirList.Add(unzipped_folder)

        Dim Found As Boolean = False
        For Each item In DirList

            If item.EndsWith(extra_path) Or item.EndsWith(extra_path + "/") Or item.EndsWith(extra_path + "\") Then
                Me.worker.ReportProgress(21, "Processamento...")
                db_file = item + backslash + database
                attachment_files = item + backslash + attachment_folder

                exists = System.IO.File.Exists(db_file)
                'Database Exists , Convert

                If exists Then
                    Found = Me.CreatePST(db_file, attachment_files)
                Else

                End If
            End If

        Next
        If Not Found Then
            Me.worker.ReportProgress(100, "N�o h� arquivos para exportar")
        End If

    End Sub
    Public Function GetFileContents(ByVal FullPath As String, _
      Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception
            ErrInfo = Ex.Message
            Return ""

        End Try
    End Function
    Public Sub addHeader(ByRef oMsg As Redemption.RDOMail, ByVal header As String, ByVal headerValue As String)
        Dim Proptag As Integer
        If header <> "" And headerValue <> "" Then
            If header = "From" Then

                Return
            End If
            'Adding To
            If header = "To" Then
               
              
                Return
            End If
            'Adding CC
            If header = "Cc" Then
         

                Return
            End If

            'Adding Subject
            If header = "Subject" Then

                Return
            End If
            'Adding Date
            If header = "Date" Then

                Return
            End If
            'Adding BCC
            If header = "Bcc" Then
                ' Proptag = oMsg.GetIDsFromNames("{00020386-0000-0000-C000-000000000046}", header)
                'Proptag = Proptag Or &H1E
                'oMsg.Fields(Proptag) = headerValue
                Return
            End If
            'Content type
            If header = "Content-Type" Then
                Me.ContentType = headerValue
                Proptag = oMsg.GetIDsFromNames("{00020386-0000-0000-C000-000000000046}", header)
                Proptag = Proptag Or &H1E
                oMsg.Fields(Proptag) = headerValue
                Return
            End If
            'Adding X-Headers


            Proptag = oMsg.GetIDsFromNames("{00020386-0000-0000-C000-000000000046}", header)
            Proptag = Proptag Or &H1E
            oMsg.Fields(Proptag) = headerValue


        End If

    End Sub
    Private Sub DeleteFiles()
        Try
            System.IO.Directory.Delete(unzipped_folder, True)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub addHeaderAndAttachment(ByVal Size As Integer, ByRef oMsg As Redemption.RDOMail)
        Dim temp As String = ""
        Dim header As String = ""
        Dim check As String = ""
        Dim dotPosition As Integer
        For Each filename As String In filenames
            Dim info As New System.IO.FileInfo(filename)


            If (info.Length - Size = 4) Then
                Me.filenamecount = Me.filenamecount + 1

                'Here we add Attachments and headers
                Dim sErr As String = ""
                Dim sContents As String = GetFileContents(filename, sErr)
                If sErr = "" Then
                    Dim position As Integer = sContents.IndexOf(vbNewLine + vbNewLine + vbNewLine)
                    Dim headers As String = sContents.Substring(0, position)
                    Dim body As String = sContents.Substring(position)
                    'Parse Headers
                    Dim headersLines As String() = headers.Split(vbNewLine)
                    For Each headersLine As String In headersLines
                        Try

                            headersLine = headersLine.Replace(vbLf, "").Replace(vbCr, "")
                            check = headersLine.Substring(0, 1)
                            If check = vbTab Or check = " " Then
                                'This is not a header!!!, just append content
                                temp = temp + vbNewLine + headersLine
                            Else
                                'This is a header
                                'Position of :
                                Me.addHeader(oMsg, header, temp)
                                dotPosition = headersLine.IndexOf(":")
                                temp = headersLine.Substring(dotPosition + 1)
                                header = headersLine.Substring(0, dotPosition)

                            End If
                        Catch ex As Exception

                            'MsgBox(headers)
                        End Try
                    Next
                    Me.addHeader(oMsg, header, temp)
                    ' Try
                    'parseBody(body, oMsg)
                    'Catch ex As Exception
                    'MsgBox(body)
                    'MsgBox(ex.Message)
                    'End Try
                End If
                'Top end if
            End If
        Next
    End Sub
    Public Function SearchForFile(ByVal attachment_files As String, ByVal filename As String, ByVal ext As String) As String
        filename = filename.Substring(filename.LastIndexOf("/") + 1)

        Dim found As String() = System.IO.Directory.GetFiles(attachment_files, filename.Replace(" ", "_") & "*" & ext, System.IO.SearchOption.AllDirectories)
        If found.Length > 0 Then
            Return found(0)
        Else
            found = System.IO.Directory.GetFiles(attachment_files, filename.Replace(" ", "%20") & "*" & ext, System.IO.SearchOption.AllDirectories)
            If found.Length > 0 Then
                Return found(0)
            End If
        End If
        Return ""
    End Function
    Private Function CreatePST(ByVal db_file As String, ByVal attachment_files As String) As Boolean

        filenames = System.IO.Directory.GetFiles(attachment_files, "fonte_da_mensagem*.txt", System.IO.SearchOption.AllDirectories)

        'Create pst file

        Dim total_count = 0
        Dim current_count = 0
        Dim targetFolder As Redemption.RDOFolder
        'Dim movedMail As Redemption.RDOMail
        Dim store As Redemption.RDOPstStore

        Dim Dnought As DateTime = #1/1/1970#

        'Read the db file
        Me.conn_string = "data source=""" + db_file + """"

        Dim sql_folders As String = "select folder from folder LIMIT 1"

        Dim sql_messages As String = "select *  from mail"

        Dim sql_total As String = "select count(*)  from mail"
        Dim rdoMapiUtils As Redemption.MAPIUtils
        rdoMapiUtils = New Redemption.MAPIUtils

        Dim conn As SQLite.SQLiteConnection = New SQLite.SQLiteConnection(conn_string)

        Dim RDOxSession As Redemption.RDOSession = CreateObject("Redemption.RDOSession")

        store = RDOxSession.LogonPstStore(Me.exported_pst, rdoStoreType.olStoreUnicode)


        Dim sql_folders_command As SQLite.SQLiteCommand = New SQLite.SQLiteCommand(sql_folders, conn)
        Dim sql_messages_command As SQLite.SQLiteCommand = New SQLite.SQLiteCommand(sql_messages, conn)
        Dim sql_total_command As SQLite.SQLiteCommand = New SQLite.SQLiteCommand(sql_total, conn)
        Dim rdr As SQLite.SQLiteDataReader = Nothing

        conn.Open()
        '  Try
        'Dim x As String = sql_folders_command.ExecuteScalar()

        '   If Not IsDBNull(x) Then
        'folder_name = x
        '   End If
        '  Catch ex As Exception

        '   End Try

        Try
            rdr = sql_messages_command.ExecuteReader()
        Catch ex As Exception

        End Try

        Try
            total_count = sql_total_command.ExecuteScalar()


        Catch ex As Exception
            total_count = 1

        End Try
        'Open existing file
        Dim subject As String = ""
        Dim i As Int16 = 0
        While rdr.Read()
            i = i + 1

            Try
                targetFolder = RDOxSession.GetDefaultFolder(rdoDefaultFolders.olFolderInbox).Folders.Add(rdr.GetString(2))

            Catch ex As Exception
                targetFolder = store.GetDefaultFolder(rdoDefaultFolders.olFolderInbox).Folders.Item(rdr.GetString(2))

            End Try
            Try
                Dim ffrom As String = rdr.GetString(8)
                ffrom = ffrom.Substring(ffrom.IndexOf("full") + 7)
                ffrom = ffrom.Substring(0, ffrom.IndexOf(";}") - 1)
                ffrom = ffrom.Substring(ffrom.IndexOf(ControlChars.Quote) + 1)
                ffrom = ffrom.Replace("&lt;", "<")
                ffrom = ffrom.Replace("&gt;", ">")
                Dim Proptag As Integer
                ' Dim omUser As Outlook.Recipient = app.Session.CreateRecipient(ffrom)


                '  omUser.Resolve()
                '  omUser.AddressEntry.Update(True)

                Dim ccString As String = rdr.GetString(11)

                ccString = ccString.Replace("&lt;", "<")
                ccString = ccString.Replace("&gt;", ">")
                Dim toString As String = rdr.GetString(10)

                toString = toString.Replace("&lt;", "<")
                toString = toString.Replace("&gt;", ">")
                toString = toString.Substring(toString.IndexOf(ControlChars.Quote) + 1)
                toString = toString.Substring(0, toString.IndexOf(";") - 1)


                current_count = current_count + 1
                ''Add messages

                'Dim oMsg As Outlook._MailItem = app.CreateItem(Outlook.OlItemType.olMailItem)
                'Dim oMsg As Redemption.SafeMailItem = CreateObject("Redemption.SafeMailItem")

                Dim oMsg As Redemption.RDOMail = targetFolder.Items.Add("IPM.Note")
                ' oMsg.MessageClass = "IPM.Note"

                subject = rdr.GetString(9)
                oMsg.Subject = subject
                oMsg.BodyFormat = Outlook.OlBodyFormat.olFormatHTML
                If ccString <> "N;" Then
                    ccString = ccString.Substring(ccString.IndexOf(ControlChars.Quote) + 1)
                    ccString = ccString.Substring(0, ccString.IndexOf(";") - 1)

                    oMsg.CC = ccString
                    'Proptag = oMsg.GetIDsFromNames("{00020386-0000-0000-C000-000000000046}", "Cc")
                    'Proptag = Proptag Or &H1E
                    'oMsg.Fields(Proptag) = ccString
                    'For Each ccUser As String In ccString.Split(",")
                    '    Dim ccUserObject As Outlook.ContactItem = app.CreateItem(Outlook.OlItemType.olContactItem)
                    '    Dim temp As String() = ccUser.Split(ControlChars.Quote + " ")
                    '    ccUserObject.Email1Address = temp.GetValue(2).ToString().Replace("<", "").Replace(">", "")
                    '    ccUserObject.Email1DisplayName = temp.GetValue(1)
                    '    ccUserObject.Move(targetFolder).Save()
                    'Next
                End If

                oMsg.HTMLBody = rdr.GetString(12)

                oMsg.ReadReceiptRequested = True
                oMsg.OriginatorDeliveryReportRequested = True

                Dim Dfinal As DateTime = Dnought.AddSeconds(rdr.GetInt64(4))

                oMsg.ExpiryTime = Dfinal
                Dim User As RDOAddressEntry = RDOxSession.CurrentUser
                Dim ffromS As String() = ffrom.Split(ControlChars.Quote + " ")
                If ffromS.Length > 1 Then
                    User.Name = ffromS.GetValue(1)
                    User.Address = ffromS.GetValue(2).ToString().Replace("<", "").Replace(">", "")
                Else
                    User.Address = ffrom
                End If
                oMsg.Sender = User
                oMsg.SentOnBehalfOf = User
                'MsgBox(omUser.AddressEntry.Address)
                'Dim ffromUserObject As Outlook.ContactItem = app.CreateItem(Outlook.OlItemType.olContactItem)

                'ffromUserObject.Move(targetFolder).Save()
                '    oMsg.SentOnBehalfOfName = ffrom
                oMsg.ReceivedTime = Dfinal
                '
                oMsg.To = toString.Replace(ControlChars.Quote, "")

                'Create to contacts
                'For Each toUser As String In toString.Split(",")
                '    Dim toUserObject As Outlook.ContactItem = app.CreateItem(Outlook.OlItemType.olContactItem)
                '    Dim temp As String() = toUser.Split(ControlChars.Quote + " ")
                '    toUserObject.Email1Address = temp.GetValue(2).ToString().Replace("<", "").Replace(">", "")
                '    toUserObject.Email1DisplayName = temp.GetValue(1)
                '    toUserObject.Move(targetFolder).Save()
                'Next


                Me.worker.ReportProgress((20 + (current_count / total_count) * 80), "Convertendo  " + current_count.ToString() + " of " + total_count.ToString())
                'Dim myAddressEntry As CDO.AddressEntry
                'oMsg.Sender = "gxdssoft@gmail.com"

                'Dim attach As String = rdr.GetString(0)
                'attach = attach.Substring(0, attach.IndexOf("encoding") - 6)
                'attach = attach.Substring(attach.IndexOf("name") + 9)
                'attach = attach.Substring(attach.IndexOf(ControlChars.Quote) + 1)
                'attach = attach.Substring(0, attach.LastIndexOf(ControlChars.Quote) - 1)
                oMsg.Sent = True
                oMsg.SentOn = Dfinal


                ' Dim sSource As String = attachment_files + attach
                ' MsgBox(sSource)
                'Dim sDisplayName As String = attach
                ' Dim sBodyLen As String = +oMsg.HTMLBody.Length
                'Attachments
                ' Dim oAttachs As Outlook.Attachments = oMsg.Attachments
                '  Dim oAttach As Outlook.Attachment
                '  oAttach = oAttachs.Add(sSource, , sBodyLen + 1, sDisplayName)

                addHeaderAndAttachment(rdr.GetInt32(13), oMsg)

                processMultiPartDb(attachment_files, oMsg, conn, i)
                Proptag = oMsg.GetIDsFromNames("{00020386-0000-0000-C000-000000000046}", "To")
                Proptag = Proptag Or &H1E
                oMsg.Fields(Proptag) = toString
                oMsg.To = toString

                oMsg.Save()
                oMsg.Send()

            Catch ex As Exception
                'MsgBox(ex.Message)
                'MsgBox(ex.StackTrace)
                'MsgBox(ex.Source)
            End Try
        End While

        store.Save()

        RDOxSession.Logoff()

        rdoMapiUtils.Cleanup()
        conn.Close()
        'MsgBox(Me.filenamecount)
        Me.worker.ReportProgress(100, "Convers�o terminada")
        Return True
    End Function

    Sub GetDirectories(ByVal StartPath As String, ByRef DirectoryList As ArrayList)
        Dim Dirs() As String = System.IO.Directory.GetDirectories(StartPath)
        DirectoryList.AddRange(Dirs)

        For Each Dir As String In Dirs
            GetDirectories(Dir, DirectoryList)
        Next
    End Sub
    Public Sub parseBody(ByVal body As String, ByRef oMsg As Redemption.RDOMail)

        If Me.ContentType.Contains("multipart") Then
            Dim boundary As String = Me.ContentType.Substring(Me.ContentType.IndexOf("boundary=") + 9).Replace(ControlChars.Quote, "")
            processMultiPart(body, boundary, oMsg)
        End If
    End Sub
    Public Sub processMultiPartDb(ByVal attachment_dir As String, ByRef oMsg As Redemption.RDOMail, ByRef conn As SQLite.SQLiteConnection, ByVal id As Int16)
        Dim sql_attachment As String = "select * from anexo WHERE id_mail = " & id
        Dim sql_attachment_command As SQLite.SQLiteCommand = New SQLite.SQLiteCommand(sql_attachment, conn)
        Dim rdr As SQLite.SQLiteDataReader = Nothing
        Dim nome_anexo As String = ""
        Dim nome, ext As String
        Try
            rdr = sql_attachment_command.ExecuteReader()
            While rdr.Read()
                nome_anexo = rdr.GetString(2)
                nome_anexo = nome_anexo.Substring(nome_anexo.IndexOf("newfilename") + 12)
                nome_anexo = nome_anexo.Replace(".php", "")
                nome = nome_anexo.Substring(0, nome_anexo.IndexOf("."))
                ext = nome_anexo.Substring(nome_anexo.IndexOf(".") + 1)
               
                Dim filepath As String = SearchForFile(attachment_dir, nome, ext)
                Dim attachments As Redemption.RDOAttachments = oMsg.Attachments
                Dim oAttach As Redemption.RDOAttachment
                oAttach = Attachments.Add(filepath)
            End While
        Catch ex As Exception
        End Try

    End Sub
    Public Sub processMultiPart(ByVal body As String, ByVal Boundary As String, ByRef oMsg As Redemption.RDOMail)

        Dim RegexObj As Regex = New Regex("Content-Type:.{0,120}name=" & ControlChars.Quote & "(.*?)" & ControlChars.Quote & ".*?" & vbNewLine & vbNewLine & "(.*?)(\n)*--", RegexOptions.Singleline)
        Dim matches As MatchCollection = RegexObj.Matches(body)
        If matches.Count() > 0 Then
            For Each match As Match In matches
                If match.Success Then
                    Dim attachments As Redemption.RDOAttachments = oMsg.Attachments

                    Dim name As String = match.Groups(1).Value
                    Dim regexSearch As String = New String(Path.GetInvalidFileNameChars()) + New String(Path.GetInvalidPathChars())

                    Dim r As Regex = New Regex(String.Format("[{0}]", Regex.Escape(regexSearch)))
                    name = r.Replace(name, "")
                    name = name.Replace("=2E", ".")
                    name = name.Replace("=", "")
                    'MsgBox(name)
                    Dim content As String = match.Groups(2).Value
                    Dim binaryData() As Byte = Convert.FromBase64String(content)

                    Dim fs As New FileStream(name, FileMode.CreateNew)
                    fs.Write(binaryData, 0, binaryData.Length)
                    fs.Close()
                    Dim oAttach As Redemption.RDOAttachment
                    oAttach = attachments.Add(name)
                    System.IO.File.Delete(name)
                   
                End If
            Next
        End If
        
        ''.Substring(Me.ContentType.IndexOf("boundary=") + 9).Replace(ControlChars.Quote, "")
        ''Match First Boundary
        'Dim inthelperstart As Integer = 0
        'Dim inthelperstop As Integer = 0
        'Dim secondBoundary As String = ""
        'Dim tmpBoundary As String = ""
        'Dim secondBody As String = ""
        'Dim content As String = ""
        'If (body.IndexOf("boundary=") > 0) Then
        '    inthelperstart = body.IndexOf("boundary=") + 9
        '    inthelperstop = body.Substring(inthelperstart).IndexOf(vbNewLine)
        '    tmpBoundary = body.Substring(inthelperstart, inthelperstop)
        '    secondBoundary = tmpBoundary
        '    body = body.Substring(body.IndexOf(secondBoundary))
        'End If
        'secondBody = body

        ' ''Search for first boundary
        'inthelperstart = body.IndexOf("--" + Boundary)

        'While (inthelperstart > 0)
        '    content = body.Substring(inthelperstart + ("--" + Boundary).Length)
        '    inthelperstop = content.IndexOf("--" + Boundary + "--")
        '    If inthelperstop > 0 Then
        '        content = content.Substring(0, inthelperstop - 1)
        '        addAttachment(content, Boundary, oMsg)
        '        body = body.Substring(inthelperstop + ("--" + Boundary + "--").Length)
        '        inthelperstart = body.IndexOf("--" + Boundary)
        '    Else
        '        inthelperstart = 0
        '    End If


        'End While

        ' ''Search for second boundary
        'inthelperstart = secondBody.IndexOf("--" + secondBoundary)
        'If secondBoundary.Length > 0 Then
        '    While (inthelperstart > 0)
        '        content = secondBody.Substring(inthelperstart + ("--" + secondBoundary).Length)
        '        inthelperstop = content.IndexOf("--" + secondBoundary + "--")

        '        If inthelperstop > 0 Then

        '            content = content.Substring(0, inthelperstop - 1)
        '            addAttachment(content, secondBoundary, oMsg)
        '            secondBody = secondBody.Substring(inthelperstop + ("--" + secondBoundary + "--").Length)
        '            inthelperstart = secondBody.IndexOf("--" + secondBoundary)
        '        Else
        '            inthelperstart = 0
        '        End If
        '    End While
        'End If


    End Sub
    Public Sub addAttachment(ByVal content As String, ByVal boundary As String, ByRef oMsg As Redemption.RDOMail)
        'Check if is html - simple
        Dim position As Integer = 0
        Dim newcontent As String = ""
        Dim cleanwhere As Integer = 2

        If content.Trim().StartsWith("Content-Type: text/plain") Then
            ''Text Plain 
            Dim lines As String() = content.Split(vbNewLine)

            For Each line As String In lines
                line = line.Replace(vbLf, "").Replace(vbCr, "")
                If line.StartsWith("Content-") Then
                    cleanwhere = cleanwhere + 1
                End If
                position = position + 1
                If position > cleanwhere Then
                    newcontent = newcontent + line + vbNewLine
                End If
            Next
            ''Generate final message and update content
            newcontent = newcontent.Substring(0, newcontent.IndexOf(boundary))

            'oMsg.Body = ConvertUtf8ToLatin1(newcontent)

            content = content.Substring(content.IndexOf(boundary) + boundary.Length + 1)
        End If
        newcontent = ""
        cleanwhere = 1
        position = 0
        If content.Trim().StartsWith("Content-Type: text/html") Then
            Dim lines As String() = content.Split(vbNewLine)
            For Each line As String In lines
                line = line.Replace(vbLf, "").Replace(vbCr, "")
                If line.StartsWith("Content-") Then
                    cleanwhere = cleanwhere + 1
                End If
                position = position + 1
                If position > cleanwhere Then
                    If line.EndsWith("=") Then
                        line = line.Substring(0, line.Length - 1).Trim()
                    End If
                    newcontent = newcontent + line
                End If
            Next
            If newcontent.IndexOf(boundary) > 0 Then
                newcontent = newcontent.Substring(0, newcontent.IndexOf(boundary) - 1)
            End If

            'oMsg.Body = ConvertUtf8ToLatin1(newcontent)

            Return
        End If
        If content.Trim().StartsWith("Content") Then
            'Get the name
            position = content.IndexOf("name=")
            Dim name As String = content.Substring(position + 6)

            position = name.IndexOf(ControlChars.Quote)
            name = name.Substring(0, position)
            name = name.Replace("=2E", ".")
            name = name.Replace("=", "")
            position = content.IndexOf(vbNewLine + vbNewLine)
            content = content.Substring(position + 2)
            If content.IndexOf(boundary) > 0 Then
                content = content.Substring(0, content.IndexOf(boundary) - 1)
            End If

            Dim attachments As Redemption.RDOAttachments = oMsg.Attachments
            Dim binaryData() As Byte = Convert.FromBase64String(content)

            Dim fs As New FileStream(name, FileMode.CreateNew)
            fs.Write(binaryData, 0, binaryData.Length)
            fs.Close()
            Dim oAttach As Redemption.RDOAttachment
            
            oAttach = attachments.Add(name)
            System.IO.File.Delete(name)
        End If
    End Sub
End Class
