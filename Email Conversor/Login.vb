Imports System
Imports System.IO
Imports System.Net
Imports System.Text

Public Class Login

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        My.Settings.user = TxtUsername.Text
        My.Settings.key = TxtPassword.Text
        My.Settings.Save()
        'Do the magic
        Dim secret As String = My.Settings.secret
        Try

      
            Dim request As WebRequest = WebRequest.Create("http://demos.hansyschmitt.com/license/license.json")
            request.Method = "POST"
            Dim postData As String = "email=" + My.Settings.user + "&license=" + My.Settings.key + "&secret=" + secret + "&count=" + My.Settings.count

            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Dim response As WebResponse = request.GetResponse()
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            dataStream.Close()
            response.Close()
       
            Dim license As License = Newtonsoft.Json.JsonConvert.DeserializeObject(Of License)(responseFromServer)

            If Not license.valid Then
                My.Settings.secret = license.secret
                My.Settings.remaining = license.left
                My.Settings.remaining_days = license.left
                My.Settings.count = 0
                My.Settings.Save()
                MsgBox(license.message)
            Else
                'Valid Right
                My.Settings.secret = license.secret
                My.Settings.remaining = license.left
                My.Settings.remaining_days = license.left
                My.Settings.count = 0
                My.Settings.Save()
                Form1.Show()
                Me.Hide()
            End If

        Catch ex As Exception

            MsgBox("Could Not Check Your License")
        End Try
        
    End Sub

    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TxtUsername.Text = My.Settings.user
        TxtPassword.Text = My.Settings.key
        Label3.Text = My.Settings.remaining
        Dim unset As Date

        If My.Settings.start.Equals(unset) Then
            My.Settings.start = Date.Today
            My.Settings.Save()


        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim result = MsgBox("Voc� tem certeza?", MsgBoxStyle.YesNo, Nothing)
        If result = DialogResult.Yes Then

            My.Settings.secret = ""
            My.Settings.remaining = 0
            My.Settings.remaining_days = 0
            My.Settings.count = 0
            My.Settings.user = ""
            My.Settings.key = ""
            My.Settings.Save()
            TxtPassword.Text = ""
            TxtUsername.Text = ""
            Label3.Text = ""
            Label5.Text = ""
        End If
    End Sub
End Class