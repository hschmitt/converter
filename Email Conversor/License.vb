Public Class License
    Private xvalid As String
    Public Property valid() As Boolean
        Get
            Return xvalid
        End Get
        Set(ByVal value As Boolean)
            xvalid = value
        End Set
    End Property
    Private xmessage As String
    Public Property message() As String
        Get
            Return xmessage
        End Get
        Set(ByVal value As String)
            xmessage = value
        End Set
    End Property
    Private xsecret As String
    Public Property secret() As String
        Get
            Return xsecret
        End Get
        Set(ByVal value As String)
            xsecret = value
        End Set
    End Property
    Private xleft As Integer
    Public Property left() As Integer
        Get
            Return xleft
        End Get
        Set(ByVal value As Integer)
            xleft = value
        End Set
    End Property

End Class
